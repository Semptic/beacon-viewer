# -*- coding: utf-8 -*-

from multiprocessing import Manager
from collections import namedtuple
import messageParser


class NameMap(dict):
    """
    This class manages the mapping of the unique bus id's
    to human readable names.
    """
    def __init__(self,bus):
        """
        Get all names which actuall registered on the bus.

        :param bus:  The dbus of interest.
        :type  bus:  dbus.Bus
        """
        self.bus = bus
        
        # Dbus has no function to get the names pairing
        # with id's only the other way.
        # This needs us too look for all common names
        # and request there id.
        for name in self.bus.list_names():
            if name[0] != ':':
                id = self.bus.get_name_owner(name)
                self[id] = name
    
    def refresh(self, name, old, new):
        """
        The D-Bus has an signal which indicates a name
        change. This signal is although sent when a object
        becomes registered or unregisterd.

        :param name:  The name (id or common) of the invoker.
        :type  name:  dbus.String

        :param old:   The old name of the invoker
                      (Empyt when registering on the bus).
        :type  old:   dbus.String

        :param new:   The new name of the invoker 
                      (Empyt when unregistering on the bus).
        :type  new:   dbus.String
        """
        if name and old and not new:
            # Object becomes removed from the bus => Name is useless
            try:
                del self[old]
            except KeyError:
                pass
        elif name and not old and new:
            # Object is new on the Bus
            if name[0] != ':':
                # Object has readable name
                self[new] = name
        elif name and old and new:
            # Name (id or readable) get changed
            if name[0] == ':':
                # Id changed
                try:
                    self[new] = self[old]
                    del self[old]
                except KeyError:
                    pass
            elif old == new:
                # Name changed
                self[old] = name

# Named tuple for parsed messages for easyer access
Message = namedtuple('Message', ['timestamp', 'sender',  'destination',
                                 'interface', 'path', 'type', 'member', 'args'])

class Parser(object):
    typeMap = {1: 'method_call',
               2: 'method_return',
               3: 'error',
               4: 'signal'}

    def __init__(self, nameMap, toIgnore=None):
        """
        Initializes the parser.
        
        :param nameMap:  Stores the unique id/common name pairs.
        :type  nameMap:  dBusParser.NameMap

        :param toIgnore: Message elements which should not stored.
        :type  toIgnore: dict
        """
        self.nameMap = nameMap
        self.parser = messageParser.Parser()

        if toIgnore == None:
            self.ignoreList = {}
        else:
            self.ignoreList = toIgnore

    def parse(self, timestamp, msg):
        """
        Pares the D-Bus message and returns the parsed
        data.

        :param timestamp:  Moment of received message. (time.time())
        :type  timestamp:  float
        
        :param msg:        Received D-Bus message.
        :type  msg:        dbus.Message

        :returns:          Parsed message if not filterd.
        :trype:            dBusParser.Message
        """
        parsedMsg = {}
        
        parsedMsg['timestamp'] = timestamp
        parsedMsg['member'] = self.parseDbusType(msg.get_member())[0]
        parsedMsg['interface'] = self.parseDbusType(msg.get_interface())[0]
        parsedMsg['path'] = self.parseDbusType(msg.get_path())[0]
        parsedMsg['args'] = self.parseDbusType(msg.get_args_list())[0]

        type_ = msg.get_type()
        parsedMsg['type'] = self.parseDbusType(
            self.typeMap.get(type_, type_)
        )[0]

        destination = msg.get_destination()
        parsedMsg['destination'] = self.parseDbusType(
            self.nameMap.get(destination, destination)
        )[0]

        sender = msg.get_sender()
        parsedMsg['sender'] = self.parseDbusType(
            self.nameMap.get(sender, sender)
        )[0]

        return self.parser.parse(Message(**parsedMsg))

    def parseDbusType(self, args):    
        if isinstance(args, list):
            out = []
            for arg in args:
                out.extend(self.parseDbusType(arg))
        elif isinstance(args, int):
            out = int(args)
        elif isinstance(args, float):
            out = float(args)
        elif isinstance(args, str):
            out = str(args)
        elif isinstance(args, tuple):
            args = list(args)
            out = self.parseDbusType(args)
        elif isinstance(args, dict):
            out = {}
            for key, value in args.items():
                key = tuple(self.parseDbusType(key))
                out[key] = self.parseDbusType(value)
        else:
            out = str(args)

        return [out]
