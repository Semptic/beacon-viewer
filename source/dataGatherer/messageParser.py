#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from collections import defaultdict, deque

class BaseParser(object):
    def __init__(self):
        self.methodCallBuffer = defaultdict(deque)

    def parse(self, message):
        if message.type == 'method_call':
            self.methodCallBuffer[
                message.sender, message.destination].append(message)
        elif message.type == 'method_return':
            try:
                caller = self.methodCallBuffer[message.destination, message.sender].popleft()
                        
                # Function starts with interface and replace '.' with '_'
                # example: 'org.kalu.fw' -> 'org_kalu_fw'
                functionName = caller.interface.replace('.', '_')
                # Next is the path and replace '/' with '_'
                # example: '/fw' -> '_fw'
                functionName += caller.path.replace('/', '_')
                # Last part is the member
                # example: 'FWX' -> '_FWX'
                functionName += '_' + caller.member
                
                # Make all lowercase
                # example:
                # 'org.kalu.fw' -> 'org_kalu_fw
                # + '/fw' -> '_fw'
                # + 'FWX' -> '_FWX'
                # => 'org_kalu_fw_fw_FWX'
                # -> 'org_kalu_fw_fw_fwx'
                functionName = functionName.lower()

                try:
                    parserfunction = getattr(self, functionName)
                    return parserfunction(message.timestamp, message.args)
                except AttributeError as e:
                    pass
                    #print e
                    #print "WARNING: {0}".format(e)
                    #print "Unknown: {0}".format(functionName)
                
            except (KeyError, IndexError):
                pass
                #print 'error'
                #print 'No method call for return found'
            
        elif message.type == 'signal':
            # Function starts with interface and replace '.' with '_'
            # example: 'org.kalu.fw' -> 'org_kalu_fw'
            functionName = message.interface.replace('.', '_')
            # Next is the path and replace '/' with '_'
            # example: '/fw' -> '_fw'
            functionName += message.path.replace('/', '_')
            # Last part is the member
            # example: 'PosChanged' -> '_PosChanged'
            functionName += '_' + message.member
            
            # Make all lowercase
            # example:
            # 'org.kalu.fw' -> 'org_kalu_fw
            # + '/fw' -> '_fw'
            # + 'PosChanged' -> '_PosChanged'
            # => 'org_kalu_fw_fw_PosChanged'
            # -> 'org_kalu_fw_fw_poschanged'
            functionName = functionName.lower()

            try:
                parserfunction = getattr(self, functionName)
                return parserfunction(message.timestamp, message.args)
            except AttributeError as e:
                pass
                #print e
                #print "WARNING: {0}".format(e)
                #print "Unknown: {0}".format(functionName)


class Parser(BaseParser):
    def __init__(self):
        super(Parser, self).__init__()
        
        self.robotData = {}

    def org_kalu_fw_fw_poschanged(self, timestamp, args):
        pass
        #print('fw PoChanged', args)
            
    def org_kalu_fw_fw_directionchanged(self, timestamp, args):
        pass
        #print('fw DirectionChanged', args)
                    
    def _parse_fw(self, type_, args):        
        if not 'aetnos' in self.robotData.keys():
            self.robotData['aetnos'] = {}
        
        if not 'fw' in self.robotData['aetnos'].keys():
            self.robotData['aetnos']['fw'] = {}
                
        old = self.robotData['aetnos']['fw'].get(type_,0)
        
        if args[0] >= old + 5 or args[0] <= old - 5:
            self.robotData['aetnos']['fw'][type_] = args[0]
            return {'aetnos': {'fw': {type_: args[0]}}}

    def org_kalu_fw_fw_fwx(self, timestamp, args):
        return self._parse_fw('x', args)

    def org_kalu_fw_fw_fwy(self, timestamp, args):
        return self._parse_fw('y', args)
            
    def org_kalu_fw_fw_fwa(self, timestamp, args):
        return self._parse_fw('a', args)

    def org_kalu_fw_fw_ultrasonicwarning(self, timestamp, args):
        pass
        #print('fw ultrasonicwarning', args)

    def org_kalu_sensorik_sensorik_senskoll(self, timestamp, args):
        # args[0] = front
        # args[1] = back
        pass
        #print('sensorik senskoll', args)

    def org_kalu_sensorik_sensorik_sensusdata(self, timestamp, args):
        pass
        #print('sensorik sensusdata', args)

    def org_kalu_sensorik_sendorik_sensumfld(self, timestamp, args):
        pass
        #print('sensorik sensumfld', args)

    def org_kalu_sensorik_sensorik_sensstat(self, timestamp, args):
        pass
        #print('sensorik sensstat', args)

    def org_kalu_sensorik_sensorik_sensstart(self, timestamp, args):
        pass
        #print('sensorik sensstart', args)
        #return {'aetnos' : {'sensorik': {'start': args}}}

    def org_kalu_rps_rps_rpspos(self, timestamp, args):
        pass
        #print('sensorik sensudata', args)


if __name__ == "__main__":
    pass
