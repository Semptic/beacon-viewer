#!/usr/bin/env python3
# -*- coding: utf-8 -*-

try:
    import gobject
except ImportError:
    from gi.repository import GObject as gobject

from dbus.mainloop.glib import DBusGMainLoop
import dbus
import time
import os
import json
import logging
import logging.handlers

from socketServer import MainServer
from storage import Storage
from dBusParser import Parser, NameMap

# Define constants
PROJECT_ROOT = os.path.abspath(os.path.dirname(os.path.realpath(__file__)))
GAME_RESET_TIMEOUT = 105
PORT = 50000

# Define glabal variables
gameStartTime = None
storage = None
magicDbus = None
nameMap = None
dbusParser = None
server = None
verbose = logging.DEBUG
_logger = logging.getLogger(__name__)


def handle_dbus_messages(bus, msg):
    """
    Handles all messages which transmitted
    over the D-Bus. Storing and parsing
    of the data only happens if strategy
    had sent that the game was started.
    If the strategy don't send the stop
    command it resets after a timeout
    of 105 seconds.
    """
    now = time.time()
    data = dbusParser.parse(now, msg)
    if data is not None:
        storage.dbusQueue.put(data)
        _logger.debug('Handle D-Bus message: {} - {}'.format(now, data))

        if gameStartTime is not None:    
            if now - gameStartTime < GAME_RESET_TIMEOUT:
                storage.parsedData.append(data)
            else:
                game_stopped()


def refresh_name(name, old_name, new_name):
    """
    When the NameOwnerChanged signal is
    emitted over D-Bus this function is called
    and refreshs the nameMap
    """
    _logger.debug('Refresh Name Map: {}, {}, {}'.format(name, old_name, new_name))

    nameMap.refresh(name, old_name, new_name)


def game_started():
    """
    This function is called when the
    strategy sends the game is started
    signal over D-Bus. When no game
    is stoped is received it resets
    manually.
    """
    _logger.debug('New Game started')

    global gameStartTime
    if len(storage.parsedData) > 0:
        _logger.debug('New Game started without stop')
        game_stopped()

    gameStartTime = time.time()
    storage.dbusQueue.put('game started')


def game_stopped():
    """
    This function get called when
    the strategy sends the game
    is done. It saves all data
    stored since start into a
    file named lastSniffedGame and
    clears the data.
    """
    _logger.debug('Game stopped')

    global gameStartTime
    gameStartTime = None
    with open('lastSniffedGame', 'w') as f:
        _logger.debug('Save data to file')
        for data in storage.parsedData:
            f.write(json.dumps(data))
            f.write('\n')

    del storage.parsedData[:]
    storage.dbusQueue.put('game stopped')


def init_logger():
    logger = logging.getLogger()

    logger.setLevel(logging.DEBUG)
    loggerfolder = os.path.join(PROJECT_ROOT, 'log')
    loggerfile = os.path.join(loggerfolder, 'dbusSniffer.log')
    
    if not os.path.exists(loggerfolder):
        os.makedirs(loggerfolder)

    handler = logging.handlers.RotatingFileHandler(
        loggerfile, maxBytes = 90 * 1000 * 1000,
        backupCount = 2
    )

    formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")

    handler.setFormatter(formatter)

    handler.setLevel(verbose)

    logger.addHandler(handler)

    _logger.info("-" * 60)
    _logger.info("Initialize new Logger")
    _logger.info("-" * 60)

    handler = logging.StreamHandler()

    handler.setLevel(verbose)

    logger.addHandler(handler)


def init_dbus():
    """
    Initalize D-Bus and connect to
    the robot bus.
    """
    _logger.info('Initializing D-Bus')

    global magicDbus
    DBusGMainLoop(set_as_default=True)

    magicBusAddress = os.getenv("DBUS_MAGIC_BUS_ADDRESS")

    try:
        if magicBusAddress is not None:
            try:
                magicDbus = dbus.bus.BusConnection(magicBusAddress)
            except dbus.exceptions.DBusException:
                _logger.warning("Magic-D-Bus nicht verfügbar")
                magicDbus = dbus.SessionBus()
        else:
            _logger.warning("DBUS_MAGIC_BUS_ADDRESS nicht gesetzt")
            magicDbus = dbus.SessionBus()
    except dbus.exceptions.DBusException:
        _logger.error('Kein D-Bus verfügbar')
        exit(1)


def init_nameMap():
    """
    Initialize the nameMapping class
    """
    _logger.info('Initializing NameMap')

    global nameMap

    nameMap = NameMap(magicDbus)


def init_parser():
    """
    Initialize the D-Bus-Parser
    """
    _logger.info('Initializing Parser')

    global dbusParser
    dBusFilter = { 'member': ['GetNameOwner', 'ListNames', 
                              'RequestName', 'Hello', 
                              'AddMatch', 'NameOwnerChanged', 
                              'NameAcquired', 'RemoveWatch', 'RemoveMatch'],
                   'sender': ['org.kalu.can', 'org.kalu.gehirn', 'org.kalu.img'],
                   'destination': ['org.kalu.can', 'org.kalu.gehirn', 'org.kalu.img']
               }

    dbusParser = Parser(nameMap, dBusFilter)


def init_storage():
    """
    Initialize the Multiproccessing storage.
    """
    _logger.info('Initializing Storage')

    global storage
    storage = Storage()

    
def init_server():
    """
    starts the server
    """
    _logger.info('Initializing Server')

    global server
    server = MainServer(PORT, storage)
    server.start()


def configure_dbus():
    """
    Configure the signals and methods we listen for.
    """
    _logger.info('Configuring D-Bus')

    magicDbus.request_name('org.kalu.dBusSniffer')
    
    # Register Signals
    magicDbus.add_signal_receiver(refresh_name, signal_name="NameOwnerChanged")
    magicDbus.add_signal_receiver(game_started, signal_name="GameStarted")
    magicDbus.add_signal_receiver(game_stopped, signal_name="GameStopped")

    # Listen to all dbus messages
    magicDbus.add_match_string("eavesdrop='true'")

    magicDbus.add_message_filter(handle_dbus_messages)


def run_dbus():
    """
    Loop forever...
    """
    _logger.info('Run D-Bus mainloop')

    mainloop = gobject.MainLoop()
    mainloop.run()

    
if __name__ == "__main__":
    init_logger()
    init_dbus()
    init_nameMap()
    init_parser()
    init_storage()
    init_server()

    configure_dbus()
    
    run_dbus()
    

