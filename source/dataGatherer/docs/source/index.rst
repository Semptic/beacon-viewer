.. GDI documentation master file, created by
   sphinx-quickstart on Thu Oct 17 13:19:30 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to GDI's documentation!
===============================

Contents:

.. toctree::
   :maxdepth: 2

   modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

