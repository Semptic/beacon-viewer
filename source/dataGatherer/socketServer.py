# -*- coding: utf-8 -*-
import sys
import socket
import struct
import json
import zlib
import time
import multiprocessing as mp
import logging
import _thread
from queue import Empty

class Server(mp.Process):
    _commands = ['handshake',
                 'get Live Data']
    
    def __init__(self):
        """
        Initializes the Serverprocess.
        """
        self._logger = logging.getLogger("{}-{}".format(__name__, self.__class__.__name__))

        self._logger.debug('Initialize Server')

        super(Server, self).__init__()
        
    def _packData(self, cmd, data):
        """
        Packs header and data for transmission.
        
        :param cmd:  Name of the command.
        :type  cmd:  str

        :param data: Data to send
        :type  data: any json packable data

        :returns:    The message header and the packed data
        :rtype:      tuple
        """
        if data:
            data_out = json.dumps(data)
            data_out = zlib.compress(data_out.encode())
        else:
            data_out = ''
        
        header_out = struct.pack('!HL', self._commands.index(cmd), len(data_out))
        
        return header_out, data_out
    
    def getHeader(self):
        """
        gets the Header of the Data
        
        :returns: header_in: received Header
        :rtype:   header_in: Header
        
        :returns: cmd_in: received Cmd
        :rtype:   cmd_in: str
        
        :returns: length_in: lenght of the Data
        :rtype:   length_in: int
        """
        header_in = self.conn.recv(6)
                
        cmd_in, length_in = struct.unpack('!HL', header_in)

        return cmd_in, length_in
    
    def getData(self, length_in):
        """
        gets the Data which is send over the socket conn
        
        :param: length_in: length of the send data
        :type:  length_in: int
        
        :returns: data_in: received Data
        :rtype: data_in: packed and json.compressed data
        """
        data_in = b''
        while len(data_in) < length_in:
            data_in += self.conn.recv(1024)

        if data_in:
            data_in = zlib.decompress(data_in)
            data_in = json.loads(data_in.decode())
            
        return data_in
    
    def getCmd(self, cmd_in):
        """
        gets which cmd to call from self._commands
        
        :param: cmd_in: str to compare with the enum
        :type:  cmd_in: str

        :returns: cmd_in: enumindex of the cmd
        :rtype:   cmd_in: int
        """
        cmd_in = self._commands[cmd_in]

        self._logger.info('Command {0} received'.format(cmd_in))
        
        return cmd_in
    
    def handleCmd(self, cmd_in, data_in):
        """
        Gets which Function is to call from cmd_in
        and gives data_in to that function.
        
        :param: cmd_in: cmd on which the function to call is decided
        :type:  cmd_in: str
        
        :param: data_in: data to pass on to the called function
        :type:  data_in: str
        
        :returns: rValue of self._packData: tupel of header_out and data_out
        :rtype: rValue of self._packData: tupel of str
        """
        handleCmd = getattr(self, 'handle_{0}'.format(cmd_in))
                                
        return self._packData(cmd_in, handleCmd(data_in))
    
    def send(self, header_out, data_out):
        """
        send Funktion
        
        :param: header_out: outgoing Header
        :type:  header_out: packed header
        
        :param: data_out: outgoing Data
        :type:  data_out: str
        """
        self.conn.sendall(header_out + data_out)
    

class MainServer(Server):
    """
    This is the MainServer class which enables the access from remote Clients over
    TCP sockets. Its intendet to run as an new Process to not block other actions.
    """
    
    def __init__(self, port, storage):
        """
        Initializes the Serverprocess.

        :param port:  Port for the Socket.
        :type  port:  int

        :param queue:   dBusQueue to get the dBusData.
        :type  queue:   multiprocessing.queue
        """
        super(MainServer, self).__init__()
        
        self._logger.debug('Initialize MainServer')
        self.storage = storage
        self.dBusQueue = storage.dbusQueue
        self._port = port
           
    def dBusDataRecv(self):
        """
        Started as new Thread, gets dBusData from the
        Grabber and puts it in the queues for the
        LiveViewServers.
        """
        while True:
            try:
                data = self.dBusQueue.get()
                if data:
                    for q in self.LiveViewQueues:
                        q.put(data)
            except Empty:
                pass
            
    def run(self):
        """
        The main routine of the server. It initializes the
        socket and listens for connection. When an
        connection is opend it respons to the received
        message from the client.
        """
        self._logger.info('MainServer started')
        
        self.LiveViewQueues = []
        
        _thread.start_new_thread(self.dBusDataRecv, tuple())
        
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        
        try:
            while True:
                try:
                    sock.bind(('', self._port))
                except socket.error as e:
                    if e.errno == 98:
                        self._logger.warning('Socket port in use, waiting')
                        time.sleep(5)
                    else:
                        raise socket.error(e)
                else:
                    break
            
            sock.listen(1)
            
            while True:
                self.conn, self.addr = sock.accept()
                
                self._logger.info('Connection ethablished with {0}'.format(self.addr))

                cmd_in, length_in = self.getHeader()

                data_in = self.getData(length_in)

                try:
                    cmd_in = self.getCmd(cmd_in)
                except IndexError:
                    self._logger.warning('Command from {0} not supported: {1}'.format(self.addr, header_in))
                    self.conn.close()
                    continue

                try:
                    header_out, data_out = self.handleCmd(cmd_in, data_in)
                except AttributeError:
                    self._logger.error(
                        'No handler function for command implemented: {0}'.format(cmd_in))
                    self.conn.close()
                    continue

        finally:
            sock.close()
            self._logger.info('Socked closed.')

    def handle_handshake(self, data_in):
        """
        Handles handshake command.
        Handshake is the first command sent
        on client start. It returns the
        server version and name.
    
        :param data_in:  Contains the Msg how to continue
        :type  data_in:  str

        :returns: data_out: should be empty right now
        :rtype:   data_out: empty
        """        
        data_out = None
        if data_in == 'LiveView':
            self._logger.info('Starting LiveViewServer for {}'.format(self.addr))

            q = mp.Queue()
            for data in self.storage.parsedData:
                q.put(data)
            self.LiveViewQueues.append(q)
            newLiveViewServer = LiveViewServer(q, self.conn)
            newLiveViewServer.start()      
            
        return data_out    

    
class LiveViewServer(Server):
    
    def __init__(self, queue, conn):
        """
        Initializes the Serverprocess.

        :param queue: Queue that is used to get the Data to send
        :type  queue: multiprocessing.queue

        :param conn:      conn for the Server to send on.
        :type  conn:      socket.conn object
        """
        super(LiveViewServer, self).__init__()
        
        self._logger.debug('Initialized LiveViewServer')
        self.LiveViewQueue = queue
        self.conn = conn        
    
        
    def run(self):
        """
        Actuall run routine of the LiveViewSever
        """

        while True:
            data = self.LiveViewQueue.get()
            
            header_out, data_out = self._packData('get Live Data', data)

            print(len(data_out))
            
            if len(data) > 100:
                print(data)
            self.send(header_out, data_out)
            
            cmd_in, length_in = self.getHeader() # Wait for acknowledge
            # ToDo handle wron ack
        

if __name__ == "__main__":
    pass
