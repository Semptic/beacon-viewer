# -*- coding: utf-8 -*-

from multiprocessing import Manager

class Storage(object):
    """
    The Storage class manages all data which should be
    available for multiple processes.
    """
    def __init__(self):
        """
        Intitializes all shared storages.
        """
        self.__manager = Manager()
        self.__parsedData = self.__manager.list()
        self.__dbusQueue = self.__manager.Queue()

    @property
    def parsedData(self):
        """
        Getter for parsedData. No setter
        to avoid overriding.
        """
        return self.__parsedData
    
    @property
    def dbusQueue(self):
        """
        Getter for dbusQueue. No stetter
        to avoid overriding.
        """
        return self.__dbusQueue

    