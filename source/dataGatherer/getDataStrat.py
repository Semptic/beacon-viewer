#!/usr/bin/env python
##############################################################################$
#   Copyright (C) 2012,2013  Thomas Vieting
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
##############################################################################$

import dbus
import gobject
from dbus.mainloop.glib import DBusGMainLoop
import threading
import signal
import sys
import os
import time

dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)

magic_bus_addr = os.getenv("DBUS_MAGIC_BUS_ADDRESS")

try:
    if magic_bus_addr is not None:
        try:
            bus = dbus.bus.BusConnection(magic_bus_addr)
        except:
            print("Magic-D-Bus nicht verfuegbar")
            bus = dbus.SessionBus()
    else:
        print("DBUS_MAGIC_BUS_ADDRESS nicht gesetzt")
        bus = dbus.SessionBus()
except:
    print "Jup, Ende. Kein D-Bus!"
    exit(1)

try:
    pwobj = bus.get_object("org.kalu.driver","/pw")
except:
    try:
        pwobj = bus.get_object("org.kalu.pw","/pw")
    except:
        print("Kein Power-Modul verfuegbar!")
        exit(1)
pw = dbus.Interface(pwobj, "org.kalu.pw")

try:
    fwobj = bus.get_object("org.kalu.driver","/fw")
except:
    try:
        fwobj = bus.get_object("org.kalu.fw", "/fw")
    except:
        print("FW modul nicht gefunden!")

fw = dbus.Interface(fwobj,"org.kalu.fw")

pw.PWSwitchOn(63)
import time

while True:
    fw.FWX()
    fw.FWY()
    fw.FWA()
    time.sleep(0.1)

