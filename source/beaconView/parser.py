#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from collections import namedtuple, defaultdict, deque

Message = namedtuple('Message', ['timestamp', 'sender',  'destination',
                                 'interface', 'path', 'type', 'member', 'args'])


class Parser(object):
    def __init__(self):
        self.methodCallBuffer = defaultdict(deque)

    def parse(self, message):
        try:
            message = Message(*message)
        except TypeError:
            return
            
        if message.type == 'method_call':
            self.methodCallBuffer[
                message.sender, message.destination].append(message)
        elif message.type == 'method_return':
            try:
                caller = self.methodCallBuffer[message.destination, message.sender].popleft()
                        
                # Function starts with interface and replace '.' with '_'
                # example: 'org.kalu.fw' -> 'org_kalu_fw'
                functionName = caller.interface.replace('.', '_')
                # Next is the path and replace '/' with '_'
                # example: '/fw' -> '_fw'
                functionName += caller.path.replace('/', '_')
                # Last part is the member
                # example: 'FWX' -> '_FWX'
                functionName += '_' + caller.member
                
                # Make all lowercase
                # example:
                # 'org.kalu.fw' -> 'org_kalu_fw
                # + '/fw' -> '_fw'
                # + 'FWX' -> '_FWX'
                # => 'org_kalu_fw_fw_FWX'
                # -> 'org_kalu_fw_fw_fwx'
                functionName = functionName.lower()

                try:
                    parserfunction = getattr(self, functionName)
                    parserfunction(message.timestamp, message.args)
                except AttributeError as e:
                    pass
                    #print e
                    #print "WARNING: {0}".format(e)
                    #print "Unknown: {0}".format(functionName)

            except (KeyError, IndexError):
                pass
                #print 'error'
                #print 'No method call for return found'
            
        elif message.type == 'signal':
            # Function starts with interface and replace '.' with '_'
            # example: 'org.kalu.fw' -> 'org_kalu_fw'
            functionName = message.interface.replace('.', '_')
            # Next is the path and replace '/' with '_'
            # example: '/fw' -> '_fw'
            functionName += message.path.replace('/', '_')
            # Last part is the member
            # example: 'PosChanged' -> '_PosChanged'
            functionName += '_' + message.member
            
            # Make all lowercase
            # example:
            # 'org.kalu.fw' -> 'org_kalu_fw
            # + '/fw' -> '_fw'
            # + 'PosChanged' -> '_PosChanged'
            # => 'org_kalu_fw_fw_PosChanged'
            # -> 'org_kalu_fw_fw_poschanged'
            functionName = functionName.lower()

            try:
                parserfunction = getattr(self, functionName)
                parserfunction(message.timestamp, message.args)
            except AttributeError as e:
                pass
                #print e
                #print "WARNING: {0}".format(e)
                #print "Unknown: {0}".format(functionName)


class Rps(object):
    def __init__(self):
        self.x = None
        self.y = None
        self.a = None
    
class Aetnos(object):
    def __init__(self):
        self.rps = Rps()

aetnos = Aetnos()

import time

class TestParser(Parser):
    def org_kalu_fw_fw_poschanged(self, timestamp, args):
        print time.time() - timestamp,  'fw PoChanged', args
            
    def org_kalu_fw_fw_directionchanged(self, timestamp, args):
        print time.time() - timestamp,  'fw DirectionChanged', args
            
    def org_kalu_fw_fw_fwx(self, timestamp, args):
        aetnos.rps.x = (timestamp, args[0])
        print time.time() - timestamp,  'fw fwx', aetnos.rps.x

    def org_kalu_fw_fw_fwy(self, timestamp, args):
        aetnos.rps.y = (timestamp, args[0])
        print time.time() - timestamp, 'fw fwy', aetnos.rps.y

    def org_kalu_fw_fw_fwa(self, timestamp, args):
        aetnos.rps.a = (timestamp, args[0])
        print time.time() - timestamp, 'fw fwa', aetnos.rps.a

    def org_kalu_fw_fw_ultrasonicwarning(self, timestamp, args):
        print time.time() - timestamp,  'fw ultrasonicwarning', args

    def org_kalu_sensorik_sensorik_senskoll(self, timestamp, args):
        # args[0] = front
        # args[1] = back
        print time.time() - timestamp,  'sensorik senskoll', args

    def org_kalu_sensorik_sensorik_sensusdata(self, timestamp, args):
        print time.time() - timestamp,  'sensorik sensusdata', args

    def org_kalu_sensorik_sendorik_sensumfld(self, timestamp, args):
        print time.time() - timestamp,  'sensorik sensumfld', args

    def org_kalu_sensorik_sensorik_sensstat(self, timestamp, args):
        print time.time() - timestamp,  'sensorik sensstat', args

    def org_kalu_sensorik_sensorik_sensstart(self, timestamp, args):
        print time.time() - timestamp,  'sensorik sensstart', args

    def org_kalu_rps_rps_rpspos(self, timestamp, args):
        print time.time() - timestamp,  'sensorik sensudata', args


if __name__ == "__main__":
    from inputSimmulation import InputSimmulation
    import time

    parser = TestParser()
    inputSim = InputSimmulation()

    i = 0
    for data in inputSim.get():
        parser.parse(data)
        i += 1

    print i
        
        
