#!/usr/bin/env python2
# -*- coding:utf-8 _*_

import json
import dbus
from Queue import Queue

class InputSimmulation:
    def get(self):
        f = open('gameData')
        
        self.buffer = Queue()

        data = f.read()
        
        f.close()

        data = json.loads(data)
        
        for d in data:
            yield d

if __name__ == '__main__':
    inputSim = InputSimmulation()
    for data in inputSim.get():
        print data
