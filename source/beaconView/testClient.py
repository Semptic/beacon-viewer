#-*- coding: utf-8 -*-
import time
import logging
import json
from socketClient import LiveViewClient
from parser import TestParser


if __name__ == "__main__":
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    handler = logging.StreamHandler()
    handler.setLevel(logging.DEBUG)
    logger.addHandler(handler)
    
    parser = TestParser()

    client = LiveViewClient('aetnosiv', 50000)
    client.connect()
    input_ = []
    try:
        for data in client.receive():
            start = time.time()
            parser.parse(data)
            print time.time() - start
            #input_.append(data)
    finally:
        client.disconnect()
        #print 'error error'
        #print 'writing'
        #f = open('{0}_gameData'.format(int(time.time())), 'w')
        #out = json.dumps(input_)
        #f.write(out)
        #f.close()
