# -*- coding: utf-8 -*-

import socket
import struct
import json
import zlib
import logging

class Client(object):
    """
    Basis Class every Client inherits of
    """
    
    def __init__(self):
        """
        Initialize the client.
        """
    def _pack_header(self, cmd_name, packet_length):
        """
        Packs the header for transmittion.

        :param cmd_name:  The command which should be transmitted.
        :type  cmd_name:  str

        :param packet_length:  The length of the transmitting data.
        :type  packet_lenght:  int

        :returns:  Packed header.
        :rtype:    byte
        """
        
        return struct.pack("!HL", self._commands.index(cmd_name), packet_length)

class LiveViewClient(Client):
    '''
    LiveView Client with connects
    to a LiveView Server to get
    constant Data of a Game
    '''
    
    _commands = ['handshake',
                 'ack']
    
    def __init__(self, ip, port):
        """
        Initialize the client.

        :param ip:  Server IP
        :type  ip:  str

        :param port:  Server port
        :type  port:  int
        
        :param cmd: Cmd which is send to the Server
        :type  cmd: str
        
        :param data_out: Message to send to the Server
        :type  data_out: str
        
        :param queue: Queue to give the Data to __main__
        :type  queue: multiprocessing.queue
        """
        super(LiveViewClient, self).__init__()
        
        self.ip = ip
        self.port = port
        self.cmd = 'handshake'
        self.data_out = 'LiveView'
        
    def connect(self):
        """
        Wrapper for communication with the Server.

        :param cmd:  The command to send.
        :type  cmd:  str

        :param data_out:  The outgoing data.
        :type data_out:   any

        :returns:  The reseved data
        :rtype:    any
        """
        
        data_out = json.dumps(self.data_out)
        data_out = zlib.compress(data_out.encode())
             
        header_out = self._pack_header(self.cmd, len(self.data_out))
        
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        self.sock.connect((self.ip, self.port))

        self.sock.sendall(header_out + data_out)    
            
    def receive(self):
        """
        Receives the Header and data_in,
        then puts data_in in the queue
        """
        try:
            data_in = 'fake'
            while data_in:

                data_in = b''
                
                header_in = self.sock.recv(6)
                header_in = struct.unpack('!HL', header_in)

                while len(data_in) < header_in[1]:
                    data_in += self.sock.recv(1024)

                self.sendAck()

                try:    
                    data_in = zlib.decompress(data_in)
                except zlib.error:
                    print 'Received data could not zlib decompressed: {0}'.format(data_in)

                try:
                    data_in = json.loads(data_in.decode())
                except ValueError:
                    print 'Received data is not json loadable: {0}'.format(data_in)

                yield data_in
                
        finally:
            self.sock.close()
            
    def sendAck(self):
        self.sock.sendall(self._pack_header('ack', 0))
            
    def disconnect(self):
        self.sock.close()
