# -*- coding: utf-8 -*-

import socket
import struct
import json
import zlib

class Client:
    '''
    Client class which connects to a server over sockets
    and communicates with it.
    '''
    
    _commands = ['handshake',
                 'getDummys',
                 'getData',
                 'rmData',
                 'open_data_stream']
     
    def __init__(self, ip, port):
        """
        Initialize the client.

        :param ip:  Server IP
        :type  ip:  str

        :param port:  Server port
        :type  port:  int
        """

        self._server_name = ''
        self._server_version = tuple()

        self.ip = ip
        self.port = port

    def _pack_header(self, cmd_name, packet_length):
        """
        Packs the header for transmittion.

        :param cmd_name:  The command which should be transmitted.
        :type  cmd_name:  str

        :param packet_length:  The length of the transmitting data.
        :type  packet_lenght:  int

        :returns:  Packed header.
        :rtype:    byte
        """
        
        return struct.pack("!HH", self._commands.index(cmd_name), packet_length)
    
    def _send(self, cmd, data_out=None):
        """
        Wrapper for communication with the Server.

        :param cmd:  The command to send.
        :type  cmd:  str

        :param data_out:  The outgoing data.
        :type data_out:   any

        :returns:  The reseved data
        :rtype:    any
        """

        if data_out is None:
            data_out = ''
        else:
            data_out = json.dumps(data_out)
            data_out = zlib.compress(data_out)

        header_out = self._pack_header(cmd, len(data_out))
        
        sock = socket.socket()
        try:
            sock.connect((self.ip, self.port))
            len_sent = sock.send(header_out)
            
            if len(data_out) > 1024:
                for i in range(int(len(data_out)/1024)):
                    len_sent = sock.send(data_out[i*1024:(i+1)*1024])

                len_sent = sock.send(data[-(len(data_out) % 1024):])
            else:
                len_sent = sock.send(data_out)

            header_in = sock.recv(4)


            header_in = struct.unpack('!HH', header_in)
            
            data_in = ''
            if header_in[1] >= 1024:
                for i in range(int(header_in[1] / 1024)):
                    data_in += sock.recv(1024)
            
            rest = header_in[1] % 1024
            
            if rest != 0:
                data_in += sock.recv(rest)
        finally:
            sock.close()
        
        if data_in:
            data_in = zlib.decompress(data_in)
            return json.loads(data_in)
        

    def _handshake(self):
        """
        Handshake message to get Server status.
        """
        data_in = self._send('handshake')
        
        if 'server-name' in data_in.keys():
            self.server_name = data_in['server-name']

        if 'server-version' in data_in.keys():
            self.server_version = tuple(data_in['server-version'])

    def getDummys(self):
        """
        Get all dummys where data is stored on the server.

        :returns: List of Logger stored on the server.
        :rtype:   list of str
        """

        return self._send('getDummys')

    def getData(self, logger):
        """
        Get all data from the specified logger stored on the server.

        :param logger:  Logger to get data for.
        :type  logger:  str

        :returns:  Data from specified logger.
        :rtype:    list of dict
        """

        return self._send('getData', logger)

    def rmData(self, logger):
        """
        Removes data from logger.

        :param logger:  Logger for removing data.
        :type  logger:  str

        :param time:    Unique key for data (timestamp)
        :type  time:    int
        """

        self._send('rmData', logger)
