import kivy
import threading
import time

kivy.require('1.0.9')

from kivy.app import App
from kivy.uix.widget import Widget
from kivy.properties import NumericProperty, ReferenceListProperty, ObjectProperty
from kivy.core.window import Window
from kivy.vector import Vector
from kivy.clock import Clock
from random import randint
from kivy.factory import Factory
import thread
import time
import kivy.uix.image

import parser
from socketClient import LiveViewClient

__version__ = '1'

class beaconApp(App):
    def build(self):
        view = beaconWidget()
        Clock.schedule_interval(view.update, 1.0/60.0)
        return view
    
class beaconWidget(Widget):
    ownRob1fw = ObjectProperty(None)
    ownRob1pbs = ObjectProperty(None)
    ownRob1sb = ObjectProperty(None)
    ownRob1sf = ObjectProperty(None)
    rps1 = ObjectProperty(None)
    
        
    def update(self, dt):
        x_1_fw = roboterData.get('aetnos', {}).get('fw', {}).get('x', 0)
        y_1_fw = roboterData.get('aetnos', {}).get('fw', {}).get('y', 0)
        a_1_fw = roboterData.get('aetnos', {}).get('fw', {}).get('a', 0)
        sb_1 = roboterData.get('aetnos', {}).get('coll', {}).get('back', 0)
        sf_1 = roboterData.get('aetnos', {}).get('coll', {}).get('front', 0)
        rps = roboterData.get('aetnos', {}).get('rps', {})
        pbs = roboterData.get('aetnos', {}).get('pbs', {})

        #Ultraschallsensoren
        if a_1_fw < 0:
            absAngle = a_1_fw * -1 + 90 #bring angle from -180/180 to 0/360
        if a_1_fw >= 0:
            absAngle = (a_1_fw * -1 + 450)%360

        if sb_1 != 0:
            self.ownRob1sb.coll()
            thread.start_new_thread(self.timer, (1, 'ownRob1sb'))
            roboterData['aetnos']['coll']['back'] = 0
        if sf_1 != 0:
            self.ownRob1sf.coll()
            thread.start_new_thread(self.timer, (1, 'ownRob1sf'))
            roboterData['aetnos']['coll']['front'] = 0
        
        #Fahrwerk
        self.ownRob1fw.move(x_1_fw, y_1_fw, a_1_fw)
        self.ownRob1sb.rotate(absAngle)
        self.ownRob1sf.rotate(absAngle)
        
        #RPS
        if rps:
            if rps['id'] == 1:
                rps_1_x = rps['x']
                rps_1_y = rps['y']
                rps_1_a = rps['a']
            self.rps1.move(rps_1_x, rps_1_y, rps_1_a)
            
        #PBS
        if pbs:
            pbs_x = pbs['x']
            pbs_y = pbs['y']
            self.ownRob1pbs.move(pbs_x, pbs_y)
            
            
    def timer(self, val, sender):
        time.sleep(val)
        if sender == 'ownRob1sb':
            self.ownRob1sb.reset()
        if sender == 'ownRob1sf':
            self.ownRob1sf.reset()
    
class OwnRobFW(Widget):
    FWxVal = NumericProperty(0)
    FWyVal = NumericProperty(0)
    fwangle = NumericProperty(0)
    def move(self, xVal, yVal, aVal):
        self.center_x = xVal / 3000 * (Window.size[0] - 20) + 10
        self.center_y = yVal / 2000 * (Window.size[1] - 20) + 10
        self.fwangle = aVal
        self.FWxVal = xVal
        self.FWyVal = yVal
        
class sensorBack(Widget):
    scolorR = NumericProperty(0)
    scolorG = NumericProperty(1)
    sangle = NumericProperty(0)
    def coll(self):
        self.scolorR = 1
        self.scolorG = 0
    def reset(self):
        self.scolorR = 0
        self.scolorG = 1
    def rotate(self, aVal):
        self.sangle = aVal       
        
class sensorFront(Widget):
    scolorR = NumericProperty(0)
    scolorG = NumericProperty(1)
    sangle = NumericProperty(0)
    def coll(self):
        self.scolorR = 1
        self.scolorG = 0
    def reset(self):
        self.scolorR = 0
        self.scolorG = 1
    def rotate(self, aVal):
        self.sangle = aVal
        
class OwnRobPBS(Widget):
    PBSxVal = NumericProperty(0)
    PBSyVal = NumericProperty(0)
    def move(self, xVal, yVal):
        self.center_x = xVal / 3000 * (Window.size[0] - 20) + 10
        self.center_y = yVal / 2000 * (Window.size[1] - 20) + 10
        self.PBSxVal = xVal
        self.PBSyVal = yVal
        
class RPS_1(Widget):
    rps1angle = NumericProperty(0)
    rps1x = NumericProperty(0)
    rps1y = NumericProperty(0)
    def move(self, xVal, yVal, aVal):
        self.center_x = xVal / 3000.0 * (Window.size[0] - 20) + 10
        self.center_y = yVal / 2000.0 * (Window.size[1] - 20) + 10
        self.rps1angle = aVal
        self.rps1x = xVal
        self.rps1y = yVal
        
Factory.register("sensorBack", sensorBack)
Factory.register("beaconWidget", beaconWidget)
Factory.register("OwnRobFW", OwnRobFW)
Factory.register("sensorFront", sensorFront)
Factory.register("OwnRobPBS", OwnRobPBS)
Factory.register("RPS_1", RPS_1)
    
lock = threading.Lock()

roboterData = {}
def getData(dt):
    global roboterData
    
    #try:
    data = clientData.next()
    #print '-'*40
    if data is not None:
        for key0, value0 in data.items():
            if key0 not in roboterData.keys():
                roboterData[key0] = {}
            for key1, value1 in value0.items():
                if key1 not in roboterData[key0].keys():
                    roboterData[key0][key1] = {}
                for key2, value2 in value1.items():
                    with lock:
                        roboterData[key0][key1][key2] = value2
    #print 'Data', data
    #except Exception as e:
        #print 'Error', e
    
    #print '-'*40
    
def startDataGetter(dt):
    Clock.schedule_interval(getData, 0.001)
    return False
    
    
if __name__ == '__main__':
    client = LiveViewClient('192.168.20.234', 50000)
    client.connect()
    
    clientData = client.receive()
    
    Clock.schedule_once(startDataGetter, 2)
    try:
        beaconApp().run()
    finally:
        client.disconnect()
