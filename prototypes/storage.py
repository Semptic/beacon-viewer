# -*- coding: utf-8 -*-

class Entity(object):
    pass

class Component(object):
    pass


class Robot(Entity):
    pass

class BigFriend(Robot):
    pass

class SmallFriend(Robot):
    pass

class BigFoe(Robot):
    pass

class SmallFoe(Robot):
    pass

class Sensor(Component):
    pass

class Actor(Component):
    pass

class Ultrasonic(Sensor):
    pass

class Gear(Sensor, Actor):
    pass


class Token(Entity):
    pass

class Fresco(Token):
    pass

class Painting(Token):
    pass
    
class Fires(Token):
    pass

class Tourches(Token):
    pass

class HeartOfFire(Token):
    pass

class FruitTree(Token):
    pass

class Fruit(Token):
    pass

class Mammoth(Token):
    pass

class Spear(Token):
    pass

class Net(Token):
    pass





