#!/usr/bin/env python3

################################################################################
#   Copyright (C) 2012,2013  Thomas Vieting
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
################################################################################

# example basictreeview.py

#import pygtk

import dbus
import os
from gi.repository import Gtk
from dbus.mainloop.glib import DBusGMainLoop


class DialogAddNode(Gtk.Dialog):

    def __init__(self, parent, initpath, typename, value):
        Gtk.Dialog.__init__(self, "My Dialog", parent, 0,
            (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
             Gtk.STOCK_OK, Gtk.ResponseType.OK))

        self.set_default_size(200, 150)

        box = self.get_content_area()

        labelpath = Gtk.Label("Pfad:");
        box.add(labelpath)

        self.entrypath = Gtk.Entry()
        self.entrypath.set_text(initpath)
        box.add(self.entrypath)

        labelpath = Gtk.Label("Typ:");
        box.add(labelpath)

        self.comboboxtype = Gtk.ComboBoxText()
        self.comboboxtype.set_entry_text_column(0)
        #self.comboboxtype.connect("changed", self.on_currency_combo_changed)
        self.comboboxtype.append_text("int")
        self.comboboxtype.append_text("float")
        self.comboboxtype.append_text("string")
        box.add(self.comboboxtype)

        labelpath = Gtk.Label("Wert:");
        box.add(labelpath)

        self.entryvalue = Gtk.Entry()
        self.entryvalue.set_text(value)
        box.add(self.entryvalue)

        self.set_modal(True)
        self.show_all()

    def GetPath(self):
        return self.entrypath.get_text()

    def GetType(self):
        return self.comboboxtype.get_active_text()

    def GetValue(self):
        return self.entryvalue.get_text()

class BrainView:

    # close the window and quit
    def delete_event(self, widget, event, data=None):
        Gtk.main_quit()
        return False

    def __init__(self):
        # Create a new window
        #self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        self.window = Gtk.Window()

        self.window.set_title("Zombie will Gehirn")

        self.window.set_size_request(400, 300)

        self.window.connect("delete_event", self.delete_event)

        # create a TreeStore with one string column to use as the model
        self.treestore = Gtk.TreeStore(str,str,str)


        # create the TreeView using treestore
        self.treeview = Gtk.TreeView(self.treestore)


        # create a CellRendererText to render the data
        self.cell = Gtk.CellRendererText()
        self.cell_editable = Gtk.CellRendererText()
        self.cell_editable.set_property('editable', True)
        self.cell_editable.connect('edited', self.CellEdited)
        self.cell_editable.set_property('editable', True)

        # add the cell to the tvcolumn and allow it to expand

        # create the TreeViewColumn to display the data
        self.tvcolumn_name = Gtk.TreeViewColumn('Knoten', self.cell, text=0)
        self.tvcolumn_type = Gtk.TreeViewColumn('Typ', self.cell, text=1)
        self.tvcolumn_value = Gtk.TreeViewColumn('Wert', self.cell_editable, text=2)

        # add tvcolumn to treeview
        self.treeview.append_column(self.tvcolumn_name)
        self.treeview.append_column(self.tvcolumn_type)
        self.treeview.append_column(self.tvcolumn_value)
        self.treeview.set_enable_tree_lines(True)
        self.treeview.set_search_column(0)
        self.treeview.set_rules_hint(True)

        self.scrolled_window = Gtk.ScrolledWindow()
        self.scrolled_window.add(self.treeview)

        self.scrolled_window.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)

        self.tvcolumn_name.set_resizable(True)
        self.tvcolumn_type.set_resizable(True)
        self.tvcolumn_value.set_resizable(True)



        # Allow sorting on the column
        self.tvcolumn_name.set_sort_column_id(0)

        # Allow drag and drop reordering of rows
        self.treeview.set_reorderable(False)

        self.button_update = Gtk.ToolButton(Gtk.STOCK_REFRESH)
        self.button_update.set_tooltip_text("Aktualisieren");
        self.button_update.connect("clicked", self.Update)
        ######################################################
        self.button_expand = Gtk.ToolButton(Gtk.STOCK_GOTO_LAST)
        self.button_expand.set_tooltip_text("Expandiere");
        self.button_expand.connect("clicked", self.Expand)
        ######################################################
        self.button_collapse = Gtk.ToolButton(Gtk.STOCK_GOTO_FIRST)
        self.button_collapse.set_tooltip_text("Kolabiere");
        self.button_collapse.connect("clicked", self.Collapse)
        ######################################################
        self.button_add = Gtk.ToolButton(Gtk.STOCK_ADD)
        self.button_add.set_tooltip_text("Hinzufuegen");
        self.button_add.connect("clicked", self.Add)
        ######################################################
        self.button_del = Gtk.ToolButton(Gtk.STOCK_REMOVE)
        self.button_del.set_tooltip_text("Entferne")
        self.button_del.connect("clicked", self.Del)

        self.top_layout = Gtk.HBox(False,2)
        self.top_layout.pack_start(self.button_update, False, False, 2)
        self.top_layout.pack_start(self.button_expand, False, False, 2)
        self.top_layout.pack_start(self.button_collapse, False, False, 2)

        self.top_layout.pack_start(self.button_add, False, False, 2)
        self.top_layout.pack_start(self.button_del, False, False, 2)

        self.main_layout = Gtk.VBox(False,2)
        self.main_layout.pack_end(self.scrolled_window, True, True, 2)
        self.main_layout.pack_start(self.top_layout, False, False, 2)

        self.window.add(self.main_layout)

        self.window.show_all()

    def Internal2ExternalPath(self, path):
        pathelements = path.split(":")

        fullreqpath = pathelements[0]
        it = self.treestore.get_iter_from_string(fullreqpath)
        resultpath = self.treestore.get_value(it, 0)

        for nodename in pathelements[1:]:
            fullreqpath += ":" + nodename
            it = self.treestore.get_iter_from_string(fullreqpath)
            resultpath += "." + self.treestore.get_value(it, 0)
        return resultpath


    def CellEdited(self, widget, path, text):
        global brain
        typename = self.treestore[path][1]
        brainpath = self.Internal2ExternalPath(path)
        #print("Brain-Pfad: ", brainpath)
        try:
            if typename == "string":
                brain.BRSetString(brainpath,text,-1)
            elif typename == "int":
                brain.BRSetInt(brainpath,int(text),-1)
            elif typename == "float":
                brain.BRSetFloat(brainpath,float(text),-1)
            elif typename == "double":
                brain.BRSetFloat(brainpath,float(text),-1)
        except:
            print("Fehler Typ bla bla")
        brain.BRRequestNodePublish(brainpath)

    def SearchNode(self, path):
        pathelements = path.split(".")

        it = self.treestore.get_iter_first()
        while it is not None:
            if self.treestore.get_value(it, 0) == pathelements[0]:
                break
            it = self.treestore.iter_next(it)
        lit = it

        for nodename in pathelements:
            if it is None:
                lit = self.treestore.append(lit, [nodename,'',''])
            else:
                while it is not None:
                    if self.treestore.get_value(it, 0) == nodename:
                        break
                    it = self.treestore.iter_next(it)
                if it is None:
                    lit = self.treestore.append(lit, [nodename,'',''])
                else:
                    lit = it
            it = self.treestore.iter_children(lit)
        return lit

    def Update(self, button=None):
        global brain
        self.treestore.clear()
        brain.BRRequestNodePublish("")

    def Clear(self):
        self.treestore.clear()

    def Add(self, button=None):
        global brain
        (tree, it) = self.treeview.get_selection().get_selected()
        brainpath = ""
        typename = ""
        value = ""
        if it is not None:
            brainpath = self.Internal2ExternalPath(str(tree.get_path(it)))
            value = tree[tree.get_path(it)][1]
            typename = tree[tree.get_path(it)][2]
        adddialog = DialogAddNode(self.window,brainpath,typename,value)
        if adddialog.run() == Gtk.ResponseType.OK:
            typename = adddialog.GetType()
            brainpath = adddialog.GetPath()
            text = adddialog.GetValue()
            try:
                if typename == "string":
                    brain.BRSetString(brainpath,text,-1)
                elif typename == "int":
                    brain.BRSetInt(brainpath,int(text),-1)
                elif typename == "float":
                    brain.BRSetFloat(brainpath,float(text),-1)
                elif typename == "double":
                    brain.BRSetFloat(brainpath,float(text),-1)
            except:
                print("Fehler Typ bla bla")
            brain.BRRequestNodePublish(brainpath)
        adddialog.destroy()


    def Del(self, button=None):
        global brain
        (tree, it) = self.treeview.get_selection().get_selected()
        if it is not None:
            brainpath = self.Internal2ExternalPath(str(self.treestore.get_path(it)))
            brain.BRDeleteObj(brainpath)
            self.treestore.remove(it)

    def Expand(self, button=None):
        self.treeview.expand_all()

    def Collapse(self, button=None):
        self.treeview.collapse_all()

    def AddEntry(self, path, typename, value):
        print(path,": ",value," : ",typename)
        node = self.SearchNode(path)
        if node is not None:
            self.treestore.set_value(node, 1, typename)
            self.treestore.set_value(node, 2, value)

def node(requestId,path,typename,value):
    global tv
    tv.AddEntry(path,typename,value)


if __name__ == "__main__":
    dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)

    magic_bus_addr = os.getenv("DBUS_MAGIC_BUS_ADDRESS")
    try:
        if magic_bus_addr is not None:
            try:
                bus = dbus.bus.BusConnection(magic_bus_addr)
            except:
                print("Magic-D-Bus nicht verfuegbar")
                bus = dbus.SessionBus()
        else:
            print("DBUS_MAGIC_BUS_ADDRESS nicht gesetzt")
            bus = dbus.SessionBus()
    except:
        print("Jup, Ende. Kein D-Bus!")
        exit(1)

    try:
        brainobj = bus.get_object("org.kalu.gehirn","/gehirn")
        brain = dbus.Interface(brainobj, "org.kalu.gehirn")
    except:
        print("Gehirn fehlt!")
        exit(2)

    bus.add_signal_receiver(node, dbus_interface="org.kalu.gehirn", signal_name="BRNodePublish")

    tv = BrainView()
    tv.Update()
    Gtk.main()

