# -*- coding: utf-8 -*-

import dbus
import gobject
from dbus.mainloop.glib import DBusGMainLoop
import sys
import time
import datetime
from collections import namedtuple


class dBusParser:
    Message = namedtuple('Message', 'time dest sender kind member path iface args')

    ignoreList = ['GetNameOwner', 'ListNames', 
                  'RequestName', 'Hello', 
                  'AddMatch', 'NameOwnerChanged', 
                  'NameAcquired', 'RemoveWatch', 'RemoveMatch']

    types = {1: 'method_call',
             2: 'method_return',
             3: 'error',
             4: 'signal'}

    def __init__(self):
        self.names = {}
        self.queue = []
        self.data = []
    
    def parse(self, times=1):
        for _ in range(times):
            self._parse(*self.queue.pop())

    def addMsg(self, timestamp, msg):
        self.queue.insert(0,(timestamp, msg))
    
    def refreshNames(self, name, old_name, new_name):
        if name and old_name and not new_name:
            if name[0] == ':':
                #print('remove: ', name)
                try:
                    del self.names[name]
                except KeyError:
                    pass
            
        elif name and not old_name and new_name:
            if name[0] != ':':
                #print('create', str(name))
                self.names[new_name] = str(name)

        elif name and old_name and new_name:
            if name[0] == ':':
                #print('refresh id', name)
                try:
                    self.names[new_name] = self.names[old_name]
                    del self.names[old_name]
                except KeyError:
                    pass

            elif old_name == new_name:
                #print('refresh name', names)
                self.names[old_name] = str(name)            

        #print('NameOwnerChanged')
        #print('-'*40)

    def initNames(self, bus):
        # Alle namen im D-Bus abgreifen uns speichern
        for name in bus.list_names():
            name = str(name)
            if name[0] != ':':
                # Die jeweilige D-Bus unique Id zu dem comman name erhalten
                id = bus.get_name_owner(name)
                self.refreshNames(name, '', id)

    def _parse(self, timestamp, msg):
        member = msg.get_member()
        
        if member in self.ignoreList: return

        time = timestamp

        dest = self.getName(msg.get_destination())

        sender = self.getName(msg.get_sender())

        kind = self.getType(msg.get_type())

        iface = msg.get_interface()

        path = msg.get_path()

        args = msg.get_args_list()

        self.data.append(self.Message(time, dest, sender, kind, member, path, iface, args))
        
        data = self.data[-1]
        #print(data)
        print('time:', datetime.datetime.fromtimestamp(data.time).strftime('%Y-%m-%d %H:%M:%S'))
        print('dest:', data.dest, 'sender:', data.sender, 'type:', data.kind, 'member:', data.member)
        print('iface:', data.iface)
        print('path:', data.path)
        print('args:', data.args)
        print('-'*40)

    def getName(self, id):
        try:
            if id[0] == ':':
                try:
                    name = self.names[id]
                except KeyError:
                    name = 'unknown({})'.format(id)
            else:
                name = id
        except TypeError:
            name = str(id)
    
        return name

    def getType(self, msg_type):
        return self.types[int(msg_type)]


def handler(bus, msg):
    global parser
    parser.addMsg(time.time(), msg)
    parser.parse()


def refreshName(name, old_name, new_name):
    global parser
    parser.refreshNames(name, old_name, new_name)


if __name__ == "__main__":
    DBusGMainLoop(set_as_default=True)

    parser = dBusParser()

    bus = dbus.SessionBus()

    bus.request_name('org.dBusSniffer.{}'.format(str(sys.argv[1])))

    parser.initNames(bus)

    bus.add_signal_receiver(refreshName, signal_name="NameOwnerChanged")


    # Filtereinstellungen
    filter = "eavesdrop='true'"
    bus.add_match_string(filter)    
    # Funktion die ausgeführt wird wenn der filter zutrifft
    bus.add_message_filter(handler)
    
    # Loop til end
    mainloop = gobject.MainLoop ()
    try:
        mainloop.run ()
    except KeyboardInterrupt:
        print('Tot...')
    
# Todo: NameOwnerChanged wird nicht gelesen
