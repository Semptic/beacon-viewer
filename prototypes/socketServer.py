# -*- coding: utf-8 -*-
import sys
import socket
import struct
import json
import zlib
import time
import multiprocessing as mp
import logging
#import storage

from database import SqliteModel

_logger = logging.getLogger(__name__)

class Server(mp.Process):
    """
    This is the Server class which enables the access from remote Clients over
    TCP sockets. Its intendet to run as an new Process to awoid blocking
    other connections (like snapCom).
    """
    
    # List of available commands, must be the same on all Clientes and
    # Servers.
    _commands = ['handshake',
                 'getDummys',
                 'getData',
                 'rmData']
    
    name = 'get Host name'
    version = (0,9)

    def __init__(self, database, port):
        """
        Initializes the Serverprocess.

        :param database:  Path to the sqlite3 database.
        :type  database:  str

        :param port:      Port for the socket to listen on.
        :type  port:      int
        """
        self.__manager = mp.Manager()
        self.__stop = self.__manager.Value(bool, False)
        self._port = port
        self.database = SqliteModel(database)
        
        super(Server, self).__init__()

    def run(self):
        """
        The main routine of the server. It initializes the
        socket and listens for connection. When an
        connection is opend it respons to the received
        message from the client.
        """
        _logger.info('Socket server started')

        sock = socket.socket()
        try:
            while True:
                try:
                    sock.bind(('', self._port))
                except socket.error as e:
                    if e.errno == 98:
                        _logger.warning('Socket port in use, waiting')
                        time.sleep(5)
                    else:
                        raise socket.error(e)
                else:
                    break

                sock.listen(1)
            
            processID = 0
            while not self.__stop.value:
                conn, addr = sock.accept()
                
                #TRYOUT AREA
                newProcess = Process(target = DataServer, args = [sock.accept(), processID])
                newProcess.start()
                processID++
                """
                #_logger.info('Connection ethablished with {0}'.format(addr))
                # First receive only the header, because it
                # defines how long the data is.
                header_in = conn.recv(4)
                
                cmd_in, length_in = struct.unpack('!HH', header_in)
                
                # Reads the data from the socket in 1024 Byte
                # packs. If the data is smaller than
                # 1024 it reads only those.
                data_in = ''
                if length_in >= 1024:
                    for i in range(length_in / 1024):
                        data_in += conn.recv(1024)
                        
                rest = length_in % 1024
                if rest != 0:
                    data_in += conn.recv(rest)
                
                # Decompress data with gzip
                data_in = zlib.decompress(data_in)

                # Convert the command it to a string
                # and close connection if command is
                # not supported
                # TODO: send error msg back
            
                try:
                    cmd_in = self._commands[cmd_in]
                except IndexError:
                    _logger.warning('Command from {0} not supported: {1}'.format(addr, header_in))
                    conn.close()
                    continue

                _logger.info('Command {0} received'.format(cmd_in))
                
                # Get the handler function of the command
                # and close connection if command hander
                # is not implemented
                # TODO: send error msg back
                try:
                    handleCmd = getattr(self, 'handle_{0}'.format(cmd_in))
                except AttributeError:
                    _logger.error('No handler function for command implemented: {0}'.format(cmd_in))
                    conn.close()
                    continue
                
                # Runs the handle function and packs the data for sending
                # handleCmd returns None or any form of json compatible
                # data
                # _packData returns the command header and the packed data
                header_out, data_out = self._packData(cmd_in, handleCmd(data_in))
            
                # Send all data at once
                # Maby change to send() and check if data is sent or not
                try:
                    conn.sendall(header_out + data_out)
                except socket.error:
                    conn.close()
                    continue
            
                # Close the connection and start listening again
                conn.close()

                _logger.info('Connection with {0} closed.'.format(addr))
                """
        finally:
            sock.close()

    def handle_handshake(self, data_in):
        """
        Handles handshake command.
        Handshake is the first command sent
        on client start. It returns the
        server version and name.
    
        :param data_in:  Should be empty.
        :type  data_in:  empty str

        :returns:        Server name and version.
        :rtype:          dict
        """
        data_out = {'server-version': self.version,
                'server-name': self.name}
        
        return data_out

    def handle_getDummys(self, data_in):
        """
        Handles getDummys command.
        If you want to interact with any
        Dummy this command must be called
        to see which Dummy is avalable on 
        this baseStation.
        
        :param data_in:  Should be empty.
        :type  data_in:  empty str

        :returns:        List of all DummyIds which have stored data on this node.
        :rtype:          list of str
        """
        data_out = self.database.getDummys()

        return data_out

    def handle_getData(self, data_in):
        """
        Handles getData command.
        Returns all data of the specified dummy.
        
        :param data_in:  packed Dummy Id
        :type  data_in:  json packed str
        
        :returns:        All stored data of the Dummy
        :rtype:          dict
        """
        dummy = json.loads(data_in)

        data_out = {}

        data_out['Status'] = self.database.getStatus(dummy)

        data_out['Hum'] = self.database.getHum(dummy)

        data_out['Temp'] = self.database.getTemp(dummy)
        
        return data_out

    def handle_rmData(self, data_in):
        """
        Handles rmData command.
        Removes all data of the specified dummy from database.

        :param data_in:  packed Dummy Id
        :type  data_in:  json packed str
        """
        dummy = json.loads(data_in)
                    
        self.database.rmDummy(dummy)

    def stop(self):
        """
        Stops the server process fail-safe
        and returns not until the process is finished.
        """
        self.__stop.value = True
        self.join()
        self.database.close()
        
    def _packData(self, cmd, data):
        """
        Packs header and data for transmission.
        
        :param cmd:  Name of the command.
        :type  cmd:  str

        :param data: Data to send
        :type  data: any json packable data

        :returns:    The message header and the packed data
        :rtype:      tuple
        """
        if data:
            data_out = json.dumps(data)
            data_out = zlib.compress(data_out)
        else:
            data_out = ''
        
        header_out = struct.pack('!HH', self._commands.index(cmd), len(data_out))
        
        return header_out, data_out
    
class DataServer(info, processID):
    _commands = ['handshake']
    
    conn, addr = info

    _logger.info('Connection ethablished with {0}'.format(addr))
    # First receive only the header, because it
    # defines how long the data is.
    header_in = conn.recv(4)
                
    cmd_in, length_in = struct.unpack('!HH', header_in)
                
    # Reads the data from the socket in 1024 Byte
    # packs. If the data is smaller than
    # 1024 it reads only those.
    data_in = ''
    if length_in >= 1024:
        for i in range(length_in / 1024):
            data_in += conn.recv(1024)
                        
    rest = length_in % 1024
    if rest != 0:
        data_in += conn.recv(rest)
                
    # Decompress data with gzip
    data_in = zlib.decompress(data_in)

    # Convert the command it to a string
    # and close connection if command is
    # not supported
    # TODO: send error msg back
            
    try:
        cmd_in = self._commands[cmd_in]
    except IndexError:
        _logger.warning('Command from {0} not supported: {1}'.format(addr, header_in))
        conn.close()
        continue

    _logger.info('Command {0} received'.format(cmd_in))
                
    # Get the handler function of the command
    # and close connection if command hander
    # is not implemented
    # TODO: send error msg back
    try:
        handleCmd = getattr(self, 'handle_{0}'.format(cmd_in))
    except AttributeError:
        _logger.error('No handler function for command implemented: {0}'.format(cmd_in))
        conn.close()
        continue
                
    # Runs the handle function and packs the data for sending
    # handleCmd returns None or any form of json compatible
    # data
    # _packData returns the command header and the packed data
    header_out, data_out = self._packData(cmd_in, handleCmd(data_in, processID))
            
    # Send all data at once
    # Maby change to send() and check if data is sent or not
    try:
        conn.sendall(header_out + data_out)
    except socket.error:
        conn.close()
        continue
            
    # Close the connection and start listening again
    conn.close()

    _logger.info('Connection with {0} closed.'.format(addr))
    
    infoQueue.put('Process Done')
    
def handle_handshake(self, data_in, processID):
    dummy = json.loads(data_in)
    
    print processID
    
    return data_out
    


if __name__ == "__main__":
    pass
