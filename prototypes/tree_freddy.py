#!/usr/bin/env python
'''
Created on 28.03.2013
@author: freddy
'''

import dbus
import os
#from gi.repository import GObject as gobject
import gobject
from dbus.mainloop.glib import DBusGMainLoop

from threading import Timer

import pygtk
pygtk.require('2.0')
import gtk
import cairo

class UsableTreeView(gtk.DrawingArea):
    '''
    Yay a usable TreeView. unlike the TreeListView thing from gtk -.-
    '''
    def __init__(self, eb):
        gtk.DrawingArea.__init__(self)
        self.connect("expose_event", self.draw)

        self.root = UTVNode(self, "Gehirn")
        self.set_size_request(200, 200)
        self.parenthesisSize = 20
        self.lineHeight = 17
        self.currentDrawingTop = 0
        self.currentSelectingTop = 0
        self.selected = None
        self.onSelectHandler = None
        self.topOffset= 0
        self.leftOffset=0

        # self.connect("button-press-event", self.mouse_down)
        eb.connect("button-press-event", self.mouse_down)

    def mouse_down(self, sender, event):
        # event.x and event.y and event.button
        if(event.type == gtk.gdk.BUTTON_PRESS):
            if(self.selected != None):
                self.selected.selected = False
            self.currentSelectingTop = 5 - self.topOffset
            tmp = self.root.get_selected(self.currentSelectingTop, event.y)
            if(tmp != None):
                tmp.selected = True
                self.selected = tmp
            self.queue_draw()
            if(self.onSelectHandler != None):
                self.onSelectHandler(self.selected)
        elif(event.type == gtk.gdk._2BUTTON_PRESS):
            self.currentSelectingTop = 5 - self.topOffset
            tmp = self.root.get_selected(self.currentSelectingTop, event.y)
            if(tmp != None):
                tmp.collapsed = not tmp.collapsed
            self.queue_draw()
        return False

    def draw(self, widget, event):
        g = widget.window.cairo_create()
        self.currentDrawingTop = 5-self.topOffset  # needed for drawing

        g.set_line_width(1)
        w = self.allocation.width
        h = self.allocation.height
        g.set_source_rgba(0, 0, 0,)

        g.rectangle(0, 0, w, h)
        g.stroke()

        g.set_source_rgb(0, 0, 0)

        g.select_font_face("Monospace", cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_NORMAL)
        g.set_font_size(13)

        self.root.draw(g, 5-self.leftOffset, self.currentDrawingTop)
        return False

    def drawnHeight(self):
        return self.root.get_height()

    def set_node(self, fullName, dataType, value):
        substrings = fullName.split('.')
        i = 1
        currentNode = self.root
        while(i < len(substrings)):
            oldNode = currentNode
            currentNode = currentNode.get_child_from_name(substrings[i])
            if(currentNode == None):
                currentNode = oldNode.add_child(substrings[i])
                self.queue_draw()
            i = i + 1
        currentNode.dataType = dataType
        currentNode.value = value
        return currentNode

    '''def get_node(self, fullName):
        substrings = fullName.split('.')
        i = 1
        currentNode = self.root
        while(i < len(substrings)):
            oldNode = currentNode
            currentNode = currentNode.get_child_from_name(substrings[i])
            if(currentNode == None):
                currentNode = oldNode.add_child(substrings[i])
            i = i+1
        return currentNode'''

    def remove_node(self, node):
        if(node.parent != None):
            node.parent.children.remove(node)
            self.queue_draw()
        return node

class Updater(object):
    def __init__(self, interval, obj, function,arg):
        self._timer     = None
        self.interval   = interval
        #self.mainWindow = mainWindow
        self.function = function
        self.obj = obj
        self.arg=arg
        self.is_running = False

    def _run(self):
        self.is_running = False
        #self.mainWindow.reloadButton_clicked(None)
        self.function(self.obj,self.arg)
        self.start()

    def start(self):
        if not self.is_running:
            self._timer = Timer(self.interval, self._run)
            self._timer.start()
            self.is_running = True

    def stop(self):
        if (self._timer != None):
            self._timer.cancel()
        self.is_running = False

class UTVNodeDataType:
    none = 'none'
    string = 'string'
    integer = 'int'
    float = 'float'


class UTVNode:
    def __init__(self, UTV, name, dataType=UTVNodeDataType.none, value=None, children=None):
        self.UTV = UTV
        self.name = name
        self.dataType = dataType
        self.value = value
        if(children == None):
            self.children = list()
        else:
            self.children = children
        self.parent = None
        self.selected = False
        self.collapsed = False

    def get_full_name(self):
        nameList = list()
        tmp = self
        while(tmp.parent != None):
            nameList.insert(0, tmp.name)
            tmp = tmp.parent
        return ".".join(nameList)

    def get_child_from_name(self, name):
        for child in self.children :
            if(child.name == name):
                return child
        return None

    def get_child_from_name_and_type(self, name, dataType):
        for child in self.children :
            if(child.name == name and child.dataType == dataType):
                return child
        return None

    def add_child(self, name, dataType=UTVNodeDataType.none, value=None):
        newNode = UTVNode(self.UTV, name , dataType, value)
        self.children.append(newNode)
        newNode.parent = self
        return newNode

    def get_height(self):
        height = self.UTV.lineHeight
        for node in self.children:
            height += node.get_height()
        return height


    def draw(self, g, left, top):
        UTVsize = self.UTV.allocation
        markLeft = self.UTV.parenthesisSize+5-self.UTV.leftOffset
        g.set_dash([1.0],1)
        while(markLeft < left):
            g.move_to(markLeft, top)
            g.line_to(markLeft, top + self.UTV.lineHeight)
            markLeft += self.UTV.parenthesisSize
        g.stroke()
        g.set_dash([1.0],0)
        if(self.selected):
            g.set_source_rgb(1, 0.5, 0.3)
            g.rectangle(left, top, UTVsize.width - left, self.UTV.lineHeight)
            g.fill()
        label = self.name
        if(len(self.children) > 0):
            if(self.collapsed):
                label = "+" + label
            else:
                label = "-" + label
        if(self.dataType != UTVNodeDataType.none):
            label += " (" + self.dataType + "): " + str(self.value)
        tbx, tby, tw, th = g.text_extents(label)[:4]
        g.move_to(left, top + th)
        g.set_source_rgb(0, 0, 0)
        g.show_text(label)
        self.UTV.currentDrawingTop += self.UTV.lineHeight
        if(not self.collapsed):
            for child in self.children:
                child.draw(g, left + self.UTV.parenthesisSize, self.UTV.currentDrawingTop)

    def get_selected(self, top, clickY):
        if(clickY > top and clickY < top + self.UTV.lineHeight):
            return self
        self.UTV.currentSelectingTop += self.UTV.lineHeight
        for child in self.children:
            tmp = child.get_selected(self.UTV.currentSelectingTop, clickY)
            if(tmp != None):
                return tmp
        return None

class MainWindow:
    def applyButton_clicked(self, sender):
        if(self.UTV.selected != None):
            #self.UTV.selected.name = self.entryVarName.get_text()
            tmpdt = self.entryType.get_active_text()
            if(tmpdt == UTVNodeDataType.none):
                self.UTV.selected.dataType = UTVNodeDataType.none
                self.UTV.selected.value = None
            else:
                self.UTV.selected.dataType = tmpdt
                self.UTV.selected.value = self.entryValue.get_text()
            self.UTV.queue_draw()
            ''''if(self.UTV.selected.dataType == UTVNodeDataType.integer):
                self.brain.BRSetInt(self.UTV.selected.get_full_name(),self.UTV.selected.value,-1)
            elif(self.UTV.selected.dataType == UTVNodeDataType.string):
                self.brain.BRSetString(self.UTV.selected.get_full_name(),self.UTV.selected.value,-1)
            elif(self.UTV.selected.dataType == UTVNodeDataType.float):
                self.brain.BRSetString(self.UTV.selected.get_full_name(),self.UTV.selected.value,-1)'''
            self.selection_changed(self.UTV.selected)  # reload data

    def removeButton_clicked(self, sender):
        if(self.UTV.selected != None):
            self.UTV.remove_node(self.UTV.selected)
            self.UTV.queue_draw()

    def addChildButton_clicked(self, sender):
        #print(self.UTV.selected.dataType)
        #print(UTVNodeDataType.none)
        #print(self.UTV.selected.dataType == UTVNodeDataType.none)

        if((self.UTV.selected != None) and (self.UTV.selected.dataType == UTVNodeDataType.none)):
            newNode = UTVNode(self.UTV, self.entryVarName.get_text())
            tmpdt = self.entryType.get_active_text()
            if(tmpdt != UTVNodeDataType.none):
                newNode.dataType = tmpdt
                newNode.value = self.entryValue.get_text()
            newNode.parent = self.UTV.selected
            self.UTV.selected.children.append(newNode)
            self.UTV.queue_draw()

    def reloadButton_clicked(self, sender):
        del self.UTV.root.children[:] #clear list
        self.brain.BRRequestNodePublish("")

    def saveButton_clicked(self, sender):
        rid = self.brain.BRBeginAtomicWrite()
        toSave = list()
        toSave.append(self.UTV.root);
        while(len(toSave)>0):
            currentNode = toSave[0]
            if(currentNode.dataType != UTVNodeDataType.none):
                path = currentNode.get_full_name()
                if(currentNode.dataType == UTVNodeDataType.integer):
                    self.brain.BRSetInt(path,int(currentNode.value),rid)
                elif(currentNode.dataType == UTVNodeDataType.string):
                    self.brain.BRSetString(path,currentNode.value,rid)
                elif(currentNode.dataType == UTVNodeDataType.float):
                    self.brain.BRSetFloat(path,float(currentNode.value),rid)
            toSave.extend(currentNode.children)
            toSave.remove(currentNode)
        self.brain.BREndAtomicWrite(rid)
        self.reloadButton_clicked(self.reloadButton)#reload saved stuff. just in case

    def autoReloadButton_clicked(self,sender):
        if(self.autoReloadButton.get_active()):
            self.updater.start()
        else:
            self.updater.stop()

    def window_resized(self, sender):
        if(not self.isResizing):
            w = self.window.allocation.width
            h = self.window.allocation.height
            self.UTV.set_size_request(w - 20, h - 70)
            self.UTVHScroll.set_size_request(w-20,10)
            self.UTVVScroll.set_size_request(10,h-70)
            '''self.container.move(self.reloadButton, left, self.reloadButton.allocation.y)
            self.container.move(self.applyButton, left, self.applyButton.allocation.y)
            self.container.move(self.removeButton, left, self.removeButton.allocation.y)
            self.container.move(self.addChildButton, left, self.addChildButton.allocation.y)
            self.container.move(self.saveButton, left, self.saveButton.allocation.y)
            self.container.move(self.entryVarName, left, self.entryVarName.allocation.y)
            self.container.move(self.entryType, left, self.entryType.allocation.y)
            self.container.move(self.entryValue, left, self.entryValue.allocation.y)'''
            self.container.move(self.UTVHScroll, 10, h - 10)
            self.container.move(self.UTVVScroll, w-10 ,60)
            self.isResizing = True
        else:
            self.isResizing = False

    def initialize_Widgets(self):
        self.window = gtk.Window()
        self.eb = gtk.EventBox()
        self.eb.add_events(gtk.gdk.BUTTON_PRESS_MASK)
        self.container = gtk.Fixed()

        self.toolbar = gtk.Toolbar()
        self.toolbar.set_orientation(gtk.ORIENTATION_HORIZONTAL)
        self.toolbar.set_style(gtk.TOOLBAR_BOTH)

        reloadIcon = gtk.Image()
        reloadIcon.set_from_stock(gtk.STOCK_REFRESH,gtk.ICON_SIZE_LARGE_TOOLBAR)
        self.reloadButton = self.toolbar.append_element(
            gtk.TOOLBAR_CHILD_BUTTON,
            None,
            "Reload",
            "Reloads the whole tree",
            "",
            reloadIcon,
            self.reloadButton_clicked,
            None)

        saveIcon = gtk.Image()
        saveIcon.set_from_stock(gtk.STOCK_SAVE,gtk.ICON_SIZE_LARGE_TOOLBAR)
        self.saveButton = self.toolbar.append_element(
            gtk.TOOLBAR_CHILD_BUTTON,
            None,
            "Save",
            "Saves the tree to the brain",
            "",
            saveIcon,
            self.saveButton_clicked,
            None)

        self.toolbar.append_space()

        addChildIcon = gtk.Image()
        addChildIcon.set_from_stock(gtk.STOCK_ADD,gtk.ICON_SIZE_LARGE_TOOLBAR)
        self.addChildButton = self.toolbar.append_element(
            gtk.TOOLBAR_CHILD_BUTTON,
            None,
            "Add child",
            "Adds a childnode to the current selected node",
            "",
            addChildIcon,
            self.addChildButton_clicked,
            None)

        removeIcon = gtk.Image()
        removeIcon.set_from_stock(gtk.STOCK_REMOVE,gtk.ICON_SIZE_LARGE_TOOLBAR)
        self.removeButton = self.toolbar.append_element(
            gtk.TOOLBAR_CHILD_BUTTON,
            None,
            "Remove",
            "Removes the selected node and all of its children from the tree",
            "",
            removeIcon,
            self.removeButton_clicked,
            None)

        applyIcon = gtk.Image()
        applyIcon.set_from_stock(gtk.STOCK_APPLY,gtk.ICON_SIZE_LARGE_TOOLBAR)
        self.applyButton = self.toolbar.append_element(
            gtk.TOOLBAR_CHILD_BUTTON,
            None,
            "Apply",
            "Apply the changes to the selected node",
            "",
            applyIcon,
            self.applyButton_clicked,
            None)

        self.entryVarName = gtk.Entry()
        self.entryVarNameElement = self.toolbar.append_element(
            gtk.TOOLBAR_CHILD_WIDGET,
            self.entryVarName,
            "Name",
            "The nodes name",
            "",
            None,
            None,
            None)

        autoReloadIcon = gtk.Image()
        autoReloadIcon.set_from_stock(gtk.STOCK_JUMP_TO,gtk.ICON_SIZE_LARGE_TOOLBAR)
        self.autoReloadButton = self.toolbar.append_element(
            gtk.TOOLBAR_CHILD_TOGGLEBUTTON,
            None,
            "Auto Reload",
            "Reload the tree every second",
            "",
            autoReloadIcon,
            self.autoReloadButton_clicked,
            None)

        self.entryType = gtk.combo_box_new_text()
        self.entryType.set_size_request(75, 30)
        self.entryType.append_text(UTVNodeDataType.none)
        self.entryType.append_text(UTVNodeDataType.string)
        self.entryType.append_text(UTVNodeDataType.integer)
        self.entryType.append_text(UTVNodeDataType.float)
        self.entryType.set_active(0)
        self.entryTypeElement = self.toolbar.append_element(
            gtk.TOOLBAR_CHILD_WIDGET,
            self.entryType,
            "Type",
            "The type of the nodes value",
            "",
            None,
            None,
            None)

        self.entryValue = gtk.Entry()
        self.entryValueElement = self.toolbar.append_element(
            gtk.TOOLBAR_CHILD_WIDGET,
            self.entryValue,
            "Value",
            "The nodes value",
            "",
            None,
            None,
            None)

        self.UTVHScroll = gtk.HScrollbar()
        adj = self.UTVHScroll.get_adjustment()
        adj.lower = 0
        adj.upper = 2000
        self.UTVHScroll.connect("value-changed", self.hscroll_changed)
        self.UTVVScroll = gtk.VScrollbar()
        self.Vadj = self.UTVVScroll.get_adjustment()
        self.Vadj.lower = 0
        self.Vadj.upper = 2000
        self.UTVVScroll.connect("value-changed", self.vscroll_changed)

        self.UTV = UsableTreeView(self.eb)
        self.UTV.onSelectHandler = self.selection_changed
        self.eb.add(self.UTV)

        self.window.add(self.container)

        self.container.put(self.toolbar,0,0)
        self.container.put(self.eb, 10, 60)
        self.container.put(self.UTVHScroll,0,5)
        self.container.put(self.UTVVScroll,0,300)
        self.container.show_all()

        self.updater = Updater(1,self,self.auto_update,None)
        self.VScrollUpdater = Updater(1,self,self.update_VScroll,None)
        self.VScrollUpdater.start()


        self.window.connect("check-resize", self.window_resized)
        self.window.connect("destroy", self.destroy)
        self.window.set_size_request(1000, 500)
        self.window.show()

    def update_VScroll(self,sec, blah):
        sec.Vadj.upper = sec.UTV.drawnHeight()

    def auto_update(self,sec,blah):
        del self.UTV.root.children[:] #clear list
        self.brain.BRRequestNodePublish("")

    def destroy(self, widget, data=None):
        self.updater.stop()
        self.VScrollUpdater.stop()
        exit(0)

    def selection_changed(self, selected):
        if(self.UTV.selected != None):
            self.entryVarName.set_text(self.UTV.selected.name)
            dataType = 0
            if(self.UTV.selected.dataType == UTVNodeDataType.string):
                dataType = 1
            elif(self.UTV.selected.dataType == UTVNodeDataType.integer):
                dataType = 2
            elif(self.UTV.selected.dataType == UTVNodeDataType.float):
                dataType = 3
            self.entryType.set_active(dataType)  # none, string, integer, float
            if(dataType > 0):
                self.entryValue.set_text(self.UTV.selected.value)
            else:
                self.entryValue.set_text("")

    def hscroll_changed(self,sender):
        self.UTV.leftOffset = self.UTVHScroll.get_adjustment().value
        self.UTV.queue_draw()

    def vscroll_changed(self,sender):
        self.UTV.topOffset = self.UTVVScroll.get_adjustment().value
        self.UTV.queue_draw()

    def rxsignal(self,requestId,path,typename,value):
        #print requestId," ",path,": ",value," : ",typename
        self.UTV.set_node("Gehirn."+path,typename,value)

    def __init__(self):
        self.isResizing = False  # used because the fucking gtk thinks a second resize is nessecary. ITS NOT!
        self.initialize_Widgets()

        dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)

        magic_bus_addr = os.getenv("DBUS_MAGIC_BUS_ADDRESS")
        try:
            if magic_bus_addr is not None:
                try:
                    bus = dbus.bus.BusConnection(magic_bus_addr)
                except:
                    print("Magic-D-Bus nicht verfuegbar")
                    bus = dbus.SessionBus()
            else:
                print("DBUS_MAGIC_BUS_ADDRESS nicht gesetzt")
                bus = dbus.SessionBus()
        except:
            print("Jup, Ende. Kein D-Bus!")
            exit(1)

        try:
            brainobj = bus.get_object("org.kalu.gehirn","/gehirn")
            self.brain = dbus.Interface(brainobj, "org.kalu.gehirn")
        except:
            print("Gehirn fehlt!")
            exit(2)

        bus.add_signal_receiver(self.rxsignal, dbus_interface="org.kalu.gehirn", signal_name="BRNodePublish")
        self.brain.BRRequestNodePublish("")

        loop = gobject.MainLoop()
        loop.run()

# print("hello")
mainWindow = MainWindow()
#gtk.main()
